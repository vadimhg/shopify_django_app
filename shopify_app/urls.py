from django.conf.urls import url
from shopify_app import views
urlpatterns = [
        url(r'^$', views.login, name='shopify_app_login'),
        url(r'^authenticate/$', views.authenticate),
        url(r'^finalize/$', views.finalize),
        url(r'^logout/$', views.logout),
]
