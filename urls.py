from django.conf.urls import include, url
from django.contrib import admin
from shopify_app import urls as shopify_app_urls
from home import urls as home_urls
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^login/', include(shopify_app_urls)),
    url(r'^', include(home_urls), name='root_path'),
]
