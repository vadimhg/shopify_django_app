from django.conf.urls import url
from home import views
urlpatterns = [
        url(r'^$', views.index, name='root_path'),
        url(r'^design/$', views.design),
        url(r'^welcome/$', views.welcome),
]
